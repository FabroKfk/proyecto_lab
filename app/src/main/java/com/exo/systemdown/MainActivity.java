package com.exo.systemdown;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView battery;
    private int level;

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Recibe el valor de la bateria.
            level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            //Setea el nivel de bateria en string, y le asigna el color gris.
            battery.setText(String.valueOf(level) + "%");
            if(level < 21){
                battery.setTextColor(Color.RED);
            }
            else{
                battery.setTextColor(Color.GRAY);
            }

            if(level == 20) {
                Context contexto = getApplicationContext();
                CharSequence text = "El dispositivo se apagará en breve.";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
            if (level < 16) {
                try {

                    String[] reboot = new String[]{"adb shell", "shutdown"};

                    Process process = Runtime.getRuntime().exec(reboot);

                    process.waitFor();

                } catch (Exception e) {

                    Context contexto = getApplicationContext();
                    CharSequence text = "El dispositivo no puede ser booteado...";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(contexto, text, duration);
                    toast.show();

                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        battery = (TextView) this.findViewById(R.id.percent);
        this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    }
}


